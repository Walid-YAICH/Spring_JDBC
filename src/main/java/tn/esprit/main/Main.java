/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.main;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import tn.esprit.conf.DataConf;
import tn.esprit.domain.Client;
import tn.esprit.mappers.ClientMapper;
/**
 * 
 * @author Walid YAICH
 */
public class Main {

	private static Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(DataConf.class);
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ctx.getBean("clientProjectJdbcTemplate");
		
		//Simple Type
		Integer count = jdbcTemplate.queryForObject("select count(*) from Client", Integer.class);
		logger.info("Nombre de clients : " + count);
		
		//Generic Maps 
		//queryForMap quand la requete devra retourner une seule ligne
		Map<String, Object> clientMap = jdbcTemplate.queryForMap("select * from Client where id=1");
		clientMap.forEach((key, value) -> logger.info(key+ " : " + value) );
		
		//Generic Maps
		//queryForList quand la requete devra retourner plusieurs lignes
		List<Map<String, Object>> clientsMaps = jdbcTemplate.queryForList("select * from Client");
		clientsMaps.forEach(client 
				-> client.forEach((key, value) -> logger.info(key+ " : " + value)));
		
		//Domain Object
		//Quand la requete devra retourner une seule ligne
		Client client = jdbcTemplate.queryForObject("select * from Client where id=1", new ClientMapper());
		logger.info("client firstName : " + client.getFirstName());
		
		//Domain Object
		//Quand la requete devra retourner plusieurs lignes
		List<Client> clients = jdbcTemplate.query("select * from Client", new ClientMapper());
		clients.forEach(clt -> logger.info("client lastName : " + clt.getLastName()));
		
		//Update queries
		jdbcTemplate.update("insert into Client (id, firstName, lastName) values (4, 'Mourad', 'Hlel')");
		jdbcTemplate.update("update Client SET lastName='tmar' where id=4");
		jdbcTemplate.update("delete from Client where id=4");
	}
}

