/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.conf;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

@Configuration
@PropertySource("classpath:db/db.properties")
public class DataConf {

	@Value("${driverClassName}")
	private String driverClassName;
	
	@Value("${url}")	
	private String url;
	
	@Value("${userName}")
	private String user;
	
	@Value("${password}")
	private String password;
	
	//Ce bean s'occupe du parsing des ${..} .
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public DataSource mariaDbDataSource() {
		//SingleConnectionDataSource this is not multi-threading capable
		SingleConnectionDataSource dataSource = new SingleConnectionDataSource();
		//it does not actually pool Connections. It just serves as simple replacement for a full-blown 
		//connection pool, implementing the same standard interface, but creating new Connections on every call.
		//DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(url);
		dataSource.setUsername(user);
		dataSource.setPassword(password);
		return dataSource;
	}
	
	@Bean
	public JdbcTemplate clientProjectJdbcTemplate(){
		return new JdbcTemplate(mariaDbDataSource());
	}
	
}
