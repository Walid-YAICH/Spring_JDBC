/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import tn.esprit.domain.Client;

public class ClientMapper implements RowMapper<Client> {
	@Override
	public Client mapRow(ResultSet rs, int rowNum) 
			throws SQLException {
		return new Client(rs.getLong("id"), 
				rs.getString("firstName"), 
				rs.getString("lastName"));
	}
}

